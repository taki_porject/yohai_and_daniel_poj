#pragma once
#include <iostream>
using namespace std;

class Card
{
public:
	Card(string type, string color);
	~Card();
	string to_string();
	string get_color() const{ return this->_color; };
	void set_color(string color){ this->_color = color; };
	string get_type() const{ return this->_type; };
	void set_type(string type){ this->_type = type; };

private:
	string _type;
	string _color;
};

Card::Card(string type = NULL, string color = NULL)
{
	this->_type = type;
	this->_color = color;
}

Card::~Card()
{
}

string Card::to_string()
{
	string cardType = NULL;
	cardType = this->_type + this->_color;
	return cardType;
}