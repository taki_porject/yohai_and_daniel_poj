#pragma once
#include <WinSock2.h>
#include <iostream>
#include <vector>
#include <queue>
//#include "user.h"
#include "card.h"
using namespace std;

class User;
class Room
{
public:
	Room(string roomName, User* Admin);
	~Room();
	
	bool add_user(User &User); //adding player to the room
	void delete_user(User &User); //removing a player form the room
	bool is_open()const{ return this->_inGame; }; //checks if the game has started
	void close();
	bool is_in_room(User const &user); //checks if the user is in the room
	bool start_game();
	bool play_turn(vector<Card> moves); //plays the turn
	bool draw_cards(int card_number);
	bool is_turn_legal(vector<Card> moves); //check if the turn is legal
	bool is_draw_legal(int num_of_cards); //check if the player can draw crads
	vector<Card> shuffle_cards(int num_of_cards); //drawing from stock
	vector<vector<Card>> shuffle_cards_start_game(int num_of_players); //deals the cards to the players
	void increase_stock(); //adds the played cards to the stock

private:
	string _room_name;
	User* _Admin;
	User* _players[4];
	bool _inGame;
	int _curPlayer;
	int _turn_modifier;
	int _draw_counter;
	Card _last_Card;
	vector<Card> _played_cards;
	queue<Card> _stock;
};



Room::~Room()
{
}