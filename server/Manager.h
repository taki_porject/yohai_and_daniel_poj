#pragma once
#include <WinSock2.h>
#include <iostream>
#include <vector>
#include <map>
#include "sqlite3.h"
#include "Room.h"
#include "user.h"
#include "card.h"
using namespace std;

class Manager
{
public:
	Manager();
	~Manager();

	bool register_user(const User &user, string password);
	void login_user(const User &user);
	bool is_exist(const User &user) const;

private:
	map<SOCKET, User> User_Map;
	vector<Room> room_vector;
	sqlite3* db;
};

Manager::Manager()
{
}

Manager::~Manager()
{
}