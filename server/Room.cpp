#include <algorithm>
#include <time.h>
#include "Room.h"

#define NUMOFCARDS 8

Room::Room(string roomName, User* Admin)
{
	Card tempCard;
	this->_room_name = roomName;
	this->_Admin = Admin;
	this->_players[0] = Admin;
	this->_players[1] = NULL;
	this->_players[2] = NULL;
	this->_players[3] = NULL;
	this->_inGame = false;
	this->_curPlayer = 0;
	this->_turn_modifier = 1;
	this->_draw_counter = 0;

	//creating the game's stock
	////adding the numbers:
	for (string i = "1"; i <= "9"; i += 1)
	{
		if (i == "2")
		{
			tempCard.set_type("$");
		}
		else
		{
			tempCard.set_type(i);
		}
		tempCard.set_color("r");
		this->_played_cards.push_back(tempCard);
		this->_played_cards.push_back(tempCard);
		tempCard.set_color("g");
		this->_played_cards.push_back(tempCard);
		this->_played_cards.push_back(tempCard);
		tempCard.set_color("b");
		this->_played_cards.push_back(tempCard);
		this->_played_cards.push_back(tempCard);
		tempCard.set_color("y");
		this->_played_cards.push_back(tempCard);
		this->_played_cards.push_back(tempCard);
	}

	////adding the TAKI card:
	tempCard.set_type("^");
	tempCard.set_color("r");
	this->_played_cards.push_back(tempCard);
	this->_played_cards.push_back(tempCard);
	tempCard.set_color("g");
	this->_played_cards.push_back(tempCard);
	this->_played_cards.push_back(tempCard);
	tempCard.set_color("b");
	this->_played_cards.push_back(tempCard);
	this->_played_cards.push_back(tempCard);
	tempCard.set_color("y");
	this->_played_cards.push_back(tempCard);
	this->_played_cards.push_back(tempCard);

	////adding the Stop card:
	tempCard.set_type("!");
	tempCard.set_color("r");
	this->_played_cards.push_back(tempCard);
	this->_played_cards.push_back(tempCard);
	tempCard.set_color("g");
	this->_played_cards.push_back(tempCard);
	this->_played_cards.push_back(tempCard);
	tempCard.set_color("b");
	this->_played_cards.push_back(tempCard);
	this->_played_cards.push_back(tempCard);
	tempCard.set_color("y");
	this->_played_cards.push_back(tempCard);
	this->_played_cards.push_back(tempCard);

	////adding the Pluse card:
	tempCard.set_type("+");
	tempCard.set_color("r");
	this->_played_cards.push_back(tempCard);
	this->_played_cards.push_back(tempCard);
	tempCard.set_color("g");
	this->_played_cards.push_back(tempCard);
	this->_played_cards.push_back(tempCard);
	tempCard.set_color("b");
	this->_played_cards.push_back(tempCard);
	this->_played_cards.push_back(tempCard);
	tempCard.set_color("y");
	this->_played_cards.push_back(tempCard);
	this->_played_cards.push_back(tempCard);

	////adding the Change diraction card:
	tempCard.set_type("<");
	tempCard.set_color("r");
	this->_played_cards.push_back(tempCard);
	this->_played_cards.push_back(tempCard);
	tempCard.set_color("g");
	this->_played_cards.push_back(tempCard);
	this->_played_cards.push_back(tempCard);
	tempCard.set_color("b");
	this->_played_cards.push_back(tempCard);
	this->_played_cards.push_back(tempCard);
	tempCard.set_color("y");
	this->_played_cards.push_back(tempCard);
	this->_played_cards.push_back(tempCard);

	////adding the Change Color card:
	tempCard.set_type("%");
	tempCard.set_color(NULL);
	this->_played_cards.push_back(tempCard);
	this->_played_cards.push_back(tempCard);
	this->_played_cards.push_back(tempCard);
	this->_played_cards.push_back(tempCard);

	////adding the Super TAKI card:
	tempCard.set_type("*");
	tempCard.set_color(NULL);
	this->_played_cards.push_back(tempCard);
	this->_played_cards.push_back(tempCard);

	this->increase_stock();
}

bool Room::add_user(User &NewUser)
{
	bool allowed = false;
	for (int i = 0; i < 4 && allowed == false; i++)
	{
		if (this->_players[i] == NULL)
		{
			this->_players[i] = &NewUser;
			allowed = true;
		}
		else
		{
			allowed = false;
		}
	}
	return allowed;
}

void Room::delete_user(User &oldUser)
{
	for (int i = 0; i < 4; i++)
	{
		if (this->_players[i] == &oldUser)
		{
			this->_players[i] = NULL;
			i = 4;
		}
	}
}

bool Room::is_in_room(User const &user)
{
	bool is_in = false;
	for (int i = 0; i < 4 && is_in == false; i++)
	{
		if (this->_players[i] == &user)
		{
			is_in = true;
		}
	}
	return is_in;
}

bool Room::is_turn_legal(vector<Card> moves)
{
	bool legal = true;
	const int num_of_cards = moves.size();
	if (moves[0].get_color == this->_last_Card.get_color() || moves[0].get_type == this->_last_Card.get_type())
	{
		for (int i = 0; (i < num_of_cards-1)&&(legal == true); i++)
		{
			if (moves[i].get_color() == moves[i + 1].get_color() || moves[i].get_type() == moves[i + 1].get_type())
			{
				legal = true;
			}
			else
			{
				legal = false;
			}
		}
	}
	else
	{
		legal = false;
	}
	return legal;
}

bool Room::is_draw_legal(int num_of_cards)
{
	bool legal = true;
	if (num_of_cards == this->_draw_counter)
	{
		legal = true;
	}
	else
	{
		legal = false;
	}
	return legal;
}

vector<Card> Room::shuffle_cards(int NumOfCards)
{
	vector<Card> draw;
	if (this->_stock.empty())
	{
		this->increase_stock;
	}
	if (this->is_draw_legal(NumOfCards))
	{
		for (int i = 0; i < NumOfCards; i++)
		{
			draw.push_back(this->_stock.front());
			this->_stock.pop();
		}
	}
	return draw;
}

void Room::increase_stock()
{
	srand(time(NULL));
	int gen_index;
	while (this->_played_cards.size() > 0)
	{
		gen_index = rand() % this->_played_cards.size();
		this->_stock.push(this->_played_cards[gen_index]);
		this->_played_cards.erase(this->_played_cards.begin() + gen_index);
	}
}

vector<vector<Card>> Room::shuffle_cards_start_game(int num_of_players)
{
	vector<vector<Card>> hands;
	for (int i = 0; i < num_of_players; i++)
	{
		for (int j = 0; j < NUMOFCARDS; j++)
		{
			hands[i][j] = this->_stock.front();
			this->_stock.pop();
		}
	}
	return hands;
}

bool Room::play_turn(vector<Card> moves)
{
	bool legal;
	Card temp;
	if (this->is_turn_legal(moves))
	{
		legal = true;
		this->_last_Card.set_color(moves.end()->get_color());
		this->_last_Card.set_type(moves.end()->get_type());

		for (int i = 0; i < moves.size(); i++)
		{
			temp.set_color(moves.begin()->get_color());
			temp.set_type(moves.begin()->get_type());
			this->_played_cards.push_back(temp);
			moves.erase(moves.begin());
		}
	}
	else
	{
		legal = false;
	}
	return legal;
}