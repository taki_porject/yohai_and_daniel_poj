#pragma once
#include <WinSock2.h>
#include <iostream>
//#include "Room.h"
using namespace std;

class Room;
class User
{
public:
	User(string username, bool isAdmin, Room* room); //c'tor
	~User(); //d'tor

	string getUserName() const{ return this->_user_name; };

	bool isAdmin() const{ return this->_is_admin; };
	void setAdmin(bool isAdmin){ this->_is_admin = isAdmin; };
private:
	string _user_name;
	Room* _room;
	bool _is_admin;
	SOCKET user_socket;
};

User::User(string username = NULL, bool isAdmin = NULL, Room* room = NULL)
{
	this->_user_name = username;
	this->_is_admin = isAdmin;
	this->_room = room;
}

User::~User()
{
}